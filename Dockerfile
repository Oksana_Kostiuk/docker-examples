FROM ruby:2.3

# Install gems
ENV APP_HOME /app_dir
RUN mkdir $APP_HOME
WORKDIR $APP_HOME
COPY Gemfile* $APP_HOME/
RUN bundle install

# Upload source
COPY . $APP_HOME

CMD ["ruby", "hello.rb"]

